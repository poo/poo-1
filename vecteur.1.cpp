#include <iostream> (cout..)
#include <iomanip> (setprecision)
#include <vector>
#include <cmath> (sqrt, pow , abs...)
using namespace std;

class Vecteur {
private:
	vector<double> vect; // On choisit ici le type vector puisque les vecteurs sont de dimension inconnue � priori et variable

public:
	// Voici quelques constructeurs utiles pour initialiser nos classe r�sultats:
	Vecteur(Vecteur const& res) = default;
	Vecteur() = default;
	Vecteur(double x, double y, double z) : vect({ x,y,z }) {}
	Vecteur(size_t taille, double valeur = 0.0) : vect(taille, valeur) {}

	// Ici ces m�thodes sont d�clar�es public puisque l'utilisateur a besoin d'y avoir acc�s
	// Permet d'ajouter un �lement � la fin du vecteur 
	void augmente(double z) {
		vect.push_back(z);
	}
	// permet pour une position int pass� en argument de modifier la valeur du vecteur


	void set_coord(int pos, double val) {
		if (pos <= 0) {
			// Ici on lance une exception de type string !
			// On suivra �galement la convention que le vecteur reste inchang� si la position est inconnue
			throw "Attention! Position inconnue dans le vecteur! Votre vecteur ne changera donc pas! HAHA..."s;
		}
		else {
			vect[pos - 1] = val;
		}
	}

	// Ici on adopte une convention d'affiche ( a b c ) pour un vecteur de dimension 3
	void affiche() const {
		cout << "( ";
		for (auto element : vect) {
			cout << element << " ";
		}
		cout << ")" << endl;
	}

	// m�thode permettant d'acc�der � la dimension d'un vecteur 
	// NB: Celle ci servira pour les fonctions qui suivent et constitue une m�thode � part enti�re pour des soucis de modularisation
	size_t getdim(Vecteur vect1) const{
		return (vect1.vect).size();
	}

	bool compare(Vecteur vect_comp) {
		if (vect.size() == getdim(vect_comp)) {
			for (size_t i(0); i < vect.size(); ++i) {
				if (vect[i] != vect_comp.vect[i]) {
					return false;
				}
			}
			return true;
		}
		else {
		// Ici on adopte la convention que si les vecteurs sont de taille diff�rente , on retourne faux!
			throw "Vecteurs de dimension diff�rente! FAUX !"s;
				return false;
		}
	}

	Vecteur addition(Vecteur const autre) const {
		// Ici on initialise le Vecteur res contenat la somme en utilisant un constructeur .
		// NB: il faut penser au destructeur mais bon on est semaine 2
		Vecteur res(vect.size(),0.0);
		if (vect.size() == getdim(autre)) {
			for (size_t i(0); i < vect.size(); ++i) {
				res.vect[i] = vect[i]+ autre.vect[i];
			}
			return res;
		}
		// Ici on lance une exception si les vecteurs n'ont pas la meme dimension
		else {
			throw " Oupsy! Vos vecteurs n'ont pas la meme dimension ! Voici � la place le vecteur autre!"s;
			// Ici on retourne autre tant que l'on a pas trouv� une meilleure mani�re de g�rer
			return autre;
		}

	}

	Vecteur oppose() const {
		Vecteur res(vect.size(),0.0);
		for (size_t j(0); j < vect.size(); ++j) {
			if (abs(vect[j]) < 10e-16) { res.vect[j] = 0.0; }
			else {
				res.vect[j] = -vect[j];
			}

		}
		return res;
	}

	Vecteur mult(double �) const {
		Vecteur res(vect.size(),0.0);
		for (size_t j(0); j < vect.size(); ++j) {
			res.vect[j] = � * vect[j];
		}
		return res;
	}

	Vecteur soustraction(Vecteur autre) const  {
		// Ici on consid�re la soustraction du vecteur de la classe par le vecteur autre
		Vecteur res(vect.size(), 0.0);
		if (vect.size() == getdim(autre)) {
			for (size_t i(0); i < vect.size(); ++i) {
				res.vect[i] = vect[i] - autre.vect[i];
			}
			return res;
		}
		// Ici on lance une exception si les dimensions ne sont pas les m�mes
		else {
			throw " Oupsy! Vos vecteurs n'ont pas la meme dimension ! Du coup, voici le vecteur autre!"s;
			// Ici on retourne autre tant que l'on a pas trouv� une meilleure mani�re de g�rer
			return autre;
		}

	}

	/* Bien que ceci ne soit pas la mani�re la plus �l�gante de d�finir le produit vectorielle,
	* nous estimons que celle ci est la plus explicite et nous l'adopterons pour des soucis de claret�*/
	Vecteur prod_vect(Vecteur autre) const {
		Vecteur res(vect.size(),0.0);
		if (vect.size() == 3) {
			if (vect.size() == getdim(autre)) {
				res.vect[0] = vect[1] * autre.vect[2] - vect[2] * autre.vect[1];
				res.vect[1] = vect[2] * autre.vect[0] - vect[0] * autre.vect[2];
				res.vect[2] = vect[0] * autre.vect[1] - vect[1] * autre.vect[0];
				return res;
			}
			else {
				throw "Produit vectoriel non d�fini!"s;
				return res;
			}
		}
		else {
			throw "Produit vectoriel impossible! Incompatible!"s;
			return res;
		}

	}

	double prod_scal(Vecteur autre) const {
		double s(0.0);
		double x;
		if (getdim(autre) == vect.size()) {
			for (size_t i(0); i < vect.size(); ++i) {
				x = vect[i] * autre.vect[i];
				s += x;
			}
			return s;
		}
	}

	Vecteur unitaire() const {
		Vecteur res(vect.size(),0.0);
		for (size_t i(0); i < vect.size(); ++i) {
			if (vect[i] >= 0) {
				res.vect[i] = 1.0;
			}
			else {
				res.vect[i] = -1.0;
			}
		} 
		return res;
	}


	double  norme() {
		double s(0);
		for (size_t j(0); j <vect.size(); ++j) {
			s += pow(vect[j], 2);
		}
		return sqrt(s);
	}


	double norme2() {
		double s(0);
		for (size_t j(0); j < vect.size(); ++j) {
			s += pow(vect[j], 2);
		}
		return s;
	}

};



// Phase de test
int main() {
	
	Vecteur vect1;
	Vecteur vect2;
	Vecteur vect3;
	Vecteur vector;
	Vecteur vect_opp;
	Vecteur somme;
	Vecteur unitary;
	Vecteur sous;
	

	
	 // v1 = (1.0, 2.0, -0.1)
	vect1.augmente(1.0); vect1.augmente(0.0); vect1.augmente(-0.1);
	vect1.set_coord(1, 2.0); // pour tester set_coord()

	// v2 = (2.6, 3.5,  4.1)
	vect2.augmente(2.6); vect2.augmente(3.5); vect2.augmente(4.1);

	// Initialisation des Vecteurs
	vect3 = vect1;
	somme = vect1;
	vect_opp = vect1;
	unitary = vect1;
	sous = vect1;
	vector = vect1;


	// Affichage des vecteurs avant op�rations
	cout << "Vecteur 1 : ";
	vect1.affiche();
	cout << endl;

	cout << "Vecteur 2 : ";
	vect2.affiche();
	cout << endl;

	cout << "Vecteur 3 : ";
	vect3.affiche();
	cout << endl;


	// Test de la premi�re partie des m�thodes:
	cout << "Le vecteur 1 est ";
	if (vect1.compare(vect2)) {
		cout << "egal au";
	}
	else {
		cout << "diff�rent du";
	}
	cout << " vecteur 2," << endl << "et est ";
	if (not vect1.compare(vect3)) {
		cout << "different du";
	}
	else {
		cout << "egal au";
	}
	cout << " vecteur 3." << endl;

	// Test des m�thodes:
	// Addition:
	somme = vect1.addition(vect2);
	cout << "La somme des vecteurs 1 et 2 composante par composante est : "; 
	somme.affiche();

	//oppos�:
	vect_opp = vect1.oppose();
	cout << "L'oppose du Vecteur 1 : ";
	vect_opp.affiche();

	// Soustraction:
	sous = vect1.soustraction(vect2);
	cout << "La soustraction des vecteurs 2 et 1 est : ";
	sous.affiche();

	//Produit vectoriel:
	vector = vect1.prod_vect(vect2);
	cout << "Le produit vectoriel entre le vecteur 1 et 2 est : ";
	vector.affiche();
	cout << endl;

	// Produit vectoriel:
	double scal;
	scal= vect1.prod_scal(vect2);
	cout << "Le produit scalaire entre le vecteur 1 et le vecteur 2 est : " << scal << endl;

	// Norme:
	double norme;
	norme= vect1.norme();
	cout << "La norme du Vecteur 1 est :" << setprecision(3) <<  norme << endl;

	// Norme au carr�:
	double norme_car;
	norme_car = vect1.norme2();
	cout << "La norme au carre du vecteur 1 est : " << setprecision(3) << norme_car << endl;


	// Vecteur unitaire associ�:
	unitary = vect1.unitaire();
	cout << "Le Vecteur unitaire du vecteur 1 est :";
	unitary.affiche();

	
	return 0;
}



